__author__ = 'pierpaolo'

import sys
import numpy as np
import imageOperations as imop
import cv2
import cv2.cv as cv
import math
import matplotlib.path as mplPath
from emd import emd
import os
from graphCC import GraphCC
from connectedcomponent import CC
import mathMethods as mm
from operator import attrgetter
from copy import deepcopy


class ObjectMatching(object):
    def __init__(self, room, door_width, text, obj_text, group_text, thresh):
        self.text_coordinates = text
        self.obj_text = obj_text
        self.group_text = group_text
        self.door_width = door_width
        self.secondary = None
        self.room = room
        self.floor = self.room.set_floor(door_width)
        self.objects = GraphCC()
        for cc in self.floor.get_indices():
            self.objects.add_node(cc)
        wall = self.room.get_wall()
        self.objects.add_node(wall)
        self.text_graph = GraphCC()
        self.groups = []
        self.closet = []
        self.objects_matching = []
        self.objects_graph = []
        self.kitchen = []
        self.toilets = []
        self.shower = []
        self.ovens = []
        self.table = []
        self.sink = []
        self.bath = []
        self.bed = []
        self.noise = []
        self.all_objects = []
        self.histThresh1 = 0.017+thresh[0]
        self.histThresh2 = 0.015+thresh[1]
        self.ratioThresh = 0.13+thresh[2]
        self.areaThresh = 0.15+thresh[3]
        self.directionThresh = 0.6+thresh[4]
        self.area_matching = []
        self.text_graphs = []
        self.doors = []

    def remove_doors_opening(self, doors):
        for door in doors.get_indices():
            # print 'look for the '+str(door.id)+' door leaf in the room '+self.room.name
            h, w = door.image.shape
            # sizes of the bounding box of the door's cc
            if h > w:
                p1 = [door.center_x+h, door.center_y]
                p2 = [door.center_x-h, door.center_y]
                radius = h
                p3 = [door.x_min-h, door.y_min]
            else:
                p1 = [door.center_x, door.center_y+w]
                p2 = [door.center_x, door.center_y-w]
                radius = w
                p3 = [door.x_min, door.y_min-w]
            inside1 = self.room.contour_cc.check_point_inside(p1[0], p1[1])
            inside2 = self.room.contour_cc.check_point_inside(p2[0], p2[1])
            # print 'p1 = '+str(p1)
            # print 'p2 = '+str(p2)
            # print 'contour extremes = '+str(self.room.contour_cc.get_extremes())
            if inside1 or inside2:
                area = radius**2*np.pi*3/8
                # print 'area semicirconferenza = '+str(area)
                for cc in self.room.graph.get_indices():
                    # print 'the cc '+str(cc.id)+' can be a door opening?'
                    # print 'area cc = '+str(cc.area)
                    min_axis = min(cc.width, cc.height)
                    max_axis = max(cc.width, cc.height)
                    if cc.area < area and min_axis < 1.05* radius and max_axis < 1.5* radius:
                        # print 'door extremes = '+str(door.get_extremes())
                        # print 'cc extremes = '+str(cc.get_extremes())
                        touch, dst = imop.touching_dilation(cc, door, width=1)
                        if touch:
                            white = cv2.countNonZero(dst)
                            # print 'white = '+str(white)
                            if white > radius:
                                graph = GraphCC()
                                graph.add_node(cc)
                                self.objects.add_node(cc)
                                self.doors.append(graph)
                                # print 'the cc '+str(cc.id)+' is a door opening'


    def search_objects(self):
        for noise in self.noise:
            # print 'look for noise '+noise.label
            self.graph_matching(noise)
        self.remove_cc_with_text()
        it = iter(sorted(self.objects_graph, key=attrgetter('label')))
        for j in range(0, len(self.objects_graph)):
            # print it.next().label
            self.graph_matching(it.next())
        self.search_secondary()

    def remove_cc_with_text(self):
        new_text = []
        for text in self.text_coordinates:
            cx, cy = text[1]
            for cc in self.room.graph.get_indices():
                if cc.id != 0 and cc.check_point_inside(cx, cy):
                    self.text_graph.add_node(cc)
                    self.objects.add_node(cc)
                    self.room.floor_graph.add_node(cc)
                    new_text.append(text)
                    self.search_objects_word_based(text, cc)

        self.text_coordinates = new_text


    def search_objects_word_based(self, text, cc):
        for obj_text in self.obj_text:
            for word in obj_text:
                if text[0].lower() == word:
                    cc1 = CC(1)
                    cc1.x_min, cc1.y_min, cc1.x_max, cc1.y_max = text[2]
                    graph = GraphCC()
                    if cc.area > 8*self.door_width**2:
                        graph.add_node(cc1)
                    else:
                        graph.add_node(cc)
                    graph.label = obj_text[0]
                    self.objects_matching.append(graph)
        for group_text in self.group_text:
            for word in group_text:
                if text[0].lower() == word:
                    cc1 = CC(1)
                    cc1.x_min, cc1.y_min, cc1.x_max, cc1.y_max = text[2]
                    img = np.zeros(((cc1.y_max-cc1.y_min), (cc1.x_max-cc1.x_min)))
                    img.fill(255)
                    cc1.image = imop.add_pad_single_pixel(img)
                    cc1.set_coordinates()
                    cc1.area1 = (cc1.x_max-cc1.x_min)*(cc1.y_max-cc1.y_min)
                    graph = GraphCC()
                    graph.add_node(cc)
                    graph.label = group_text[0]
                    if cc.area < 8*self.door_width**2:
                        self.area_matching.append(graph)
                    graph.add_node(cc1)
                    self.text_graphs.append(graph)
                    self.search_area(graph, sec=False)

    def search_area(self, graph, sec):
        group = self.get_group(graph.label)
        if len(group)>0:
            if sec:
                # print 'cerco oggetti secondari in '+group[0]
                list_graph = self.secondary
                b = len(group)
                i = b-1
                while group[i][0:3] == 'sec':
                    i-=1
                a = i+1
                d = 3
            else:
                a = 1
                i = 1
                while group[i][0:3] != 'sec':
                    i+=1
                b = i
                d = 0
                list_graph = self.objects_graph
            nodes = graph.get_all_nodes()
            area_graph = GraphCC()
            area_graph.label = graph.label
            for i in range(a, b):
                for objGraph in list_graph:
                    if objGraph.label[0:3] == group[i][0+d:3+d]:
                        # print 'cerco lo oggetto '+objGraph.label+' del gruppo '+group[0]
                        for cc in self.room.graph.get_indices():
                                over_wall = imop.over_wall(self.room.graph.img_red_wall, cc, nodes[1])
                                isin_cond = False
                                if not self.objects.isIn(cc):
                                    isin_cond = True
                                if isin_cond and not over_wall:
                                    found, imgGraph = self.graph_matching2(objGraph, cc)
                                    if found:
                                        for cc2 in imgGraph.get_indices():
                                            area_graph.add_node(cc2)
            if area_graph.size() > 0 and not sec:
                self.area_matching.append(area_graph)


    def get_group(self, name):
        for group in self.groups:
            if group[0] == name:
                return group
        return []

    def get_all_objects(self):
        # for graph in self.toilets:
        #     self.all_objects.append(graph)
        # for graph in self.sink:
        #     self.all_objects.append(graph)
        # for graph in self.bath:
        #     self.all_objects.append(graph)
        # for graph in self.ovens:
        #     self.all_objects.append(graph)
        noise = []
        for graph in self.table:
            self.all_objects.append(graph)
        for graph in self.objects_matching:
            if 'noise' not in graph.label:
                self.all_objects.append(graph)
            else:
                noise.append(graph)
        return self.all_objects, noise




    def add_internal_cc(self, graph, cc):
        for edge in self.room.graph.get_edges_to(cc):
            id1, id2 = edge.get_nodes_id()
            if (edge.inside%2 == 1 and id2 == cc.id) or (edge.inside%2 == 0 and edge.inside != 0 and id1 == cc.id):
                if id1 == cc.id:
                    graph.add_node(edge.node2)
                    self.objects.add_node(edge.node2)
                else:
                    graph.add_node(edge.node1)
                    self.objects.add_node(edge.node1)
        return graph


    def get_group_from_obj(self, name):
        groups = []
        for group in self.groups:
            for lab in group:
                if lab[0:3] == name[0:3]:
                    groups.append(group)
        return groups

    def search_secondary(self):
        for imgGraph in self.objects_matching:
            # print 'cerco oggetti secondari dallo oggetto '+imgGraph.label
            groups = self.get_group_from_obj(imgGraph.label)
            main_node = imgGraph.getMaxNode()
            for group in groups:
                end = len(group)-1
                secondary = []
                while group[end][0:3] == 'sec':
                    secondary.append(group[end][3:])
                    end-=1
                for i in range(1, len(secondary)+1):
                    sec = secondary[-i]
                    for obj in self.secondary:
                        if obj.label[0:3] == sec[0:3]:
                            for cc in self.room.graph.get_indices():
                                over_wall = imop.over_wall(self.room.graph.img_red_wall, cc, main_node)
                                if not self.objects.isIn(cc) and not over_wall:# and obj.rule != 'inside':
                                    # print 'cerco lo oggetto secondario '+obj.label+' a partire dalla cc '+str(cc.id)
                                    self.graph_matching2(obj, cc)
        for text_graph in self.text_graphs:
            self.search_area(text_graph, sec=True)


    def find_table(self):
        thresh1 = 0.25*self.door_width**2
        thresh2 = 5*self.door_width**2
        for cc in self.room.graph.get_indices():
            if self.room.linked_to_floor(cc):
                floor_cond = True
            else:
                floor_cond = False
            # # print 'floor condition for cc '+str(cc.id)+' = '+str(floor_cond)
            if not self.objects.isIn(cc) and floor_cond and thresh1 <= cc.area <= thresh2:
                count = 0
                inside = False
                for edge in self.room.graph.get_edges_to(cc):

                    if edge.type == 'touch':
                        count+=1
                        break
                    else:
                        id1, id2 = edge.get_nodes_id()
                        if id1 == cc.id:
                            cc2 = edge.node2
                        else:
                            cc2 = edge.node1
                        if self.room.floor_graph.isIn(cc2) and 1 <= edge.inside <= 2:
                            inside = True
                if inside and count == 0:
                    graph = GraphCC()
                    graph.add_node(cc)
                    graph.label = 'Special Table Function'
                    self.table.append(graph)
                    self.objects.add_node(cc)

    def check_number_cc(self, objGraph):
        num_cc = self.room.graph.size()-self.objects.size()
        if num_cc >= len(objGraph.main_nodes):
            return True
        return False

    def check_main_nodes_are_in(self, foundObject, objGraph):
        if objGraph.extend:
            return True
        else:
            for cc in objGraph.main_nodes:
                if not foundObject.isIn(cc):
                    return False
        return True

    def calculate_ratio_cond(self, cc, i):
        min_d, point1 = mm.calculate_min_distance(cc.contourNone[i], cc.cx[i], cc.cy[i])
        max_d, point2 = mm.calculate_max_distance(cc.contourSimple[i], cc.cx[i], cc.cy[i])
        if max_d > 0:
            ratio = min_d/max_d
        else:
            ratio = 0
        return ratio


    def match_internal_histograms(self, cc_img, cc_obj):
        n = cc_obj.contours_number()
        m = cc_img.contours_number()
        if n <= 2 and m <= 2:
            return True
        n = n/2
        j = []
        for i in range(1, len(cc_obj.histogram)):
            for k in range(1, len(cc_img.histogram)):
                emdist1 = emd(cc_img.histogram[k], cc_obj.histogram[i])
                emdist2 = emd(cc_img.histogram2[k], cc_obj.histogram2[i])
                if emdist1 < self.histThresh1 and emdist2 < self.histThresh2:
                    emd_cond = True
                else:
                    emd_cond = False
                ratio1 = self.calculate_ratio_cond(cc_img, k)
                ratio2 = self.calculate_ratio_cond(cc_obj, i)
                ratio = abs(ratio1-ratio2)
                if ratio < self.ratioThresh:
                    ratio_cond = True
                else:
                    ratio_cond = False
                if k not in j and emd_cond and ratio_cond:
                    j.append(k)
                    if len(j) >= n:
                        return True
        return False

    def check_node_attributes(self, cc_obj, cc_img):
        num_img = cc_img.contours_number()
        num_obj = cc_obj.contours_number()
        contour_cond = False
        if num_obj <= num_img < 2*num_obj:
            contour_cond = True
        emd_cond = False
        if cc_obj.shape == cc_img.shape and cc_img.shape != 'unknown':
            emd_cond = True
        else:
            emdist1 = emd(cc_img.histogram[0], cc_obj.histogram[0])
            emdist2 = emd(cc_img.histogram2[0], cc_obj.histogram2[0])
            if emdist1 < self.histThresh1 and emdist2 < self.histThresh2:
                emd_cond = True
            #else:
                # print 'emd distance = '+str(emdist1)+' and '+str(emdist2)
        ratio = abs(cc_img.ratio - cc_obj.ratio)
        if ratio < self.ratioThresh:
            ratio_cond = True
        else:
            ratio_cond = False
        internal = self.match_internal_histograms(cc_img, cc_obj)
        # print 'condizioni emd, ratio, internal, contour = '
        # print str(emd_cond)
        # print str(ratio)
        # print str(internal)
        # print str(contour_cond)#+': num_img = '+str(num_img)+', num_obj = '+str(num_obj)
        if emd_cond and ratio_cond and internal and contour_cond:
            return True
        return False

    def extend_noise(self, imgGraph, objGraph):
        # print 'estendo il rumore '+str(objGraph.label)
        graph = GraphCC()
        mainNode = objGraph.getMaxNode()
        foundObject = GraphCC()
        foundObject.add_node(mainNode)
        for cc in imgGraph.get_indices():
            graph.add_node(cc)
        for cc in graph.get_indices():
            found = self.__matchAttributes(mainNode, cc, foundObject, imgGraph, objGraph, size=1)
            # if found:
            #     self.extend_noise(imgGraph, objGraph)
            # for edge in self.room.graph.get_edges_to(cc):
            #     id1_a, id2_a = edge.get_nodes_id()
            #     inside1 = 0
            #     if id1_a == edge.node.id:
            #         cc_img = edge.node2
            #         inside1 = edge.inside
            #     else:
            #         cc_img = edge.node1
            #         if edge.inside%2 == 1:
            #             inside1 = edge.inside+1
            #         elif edge.inside != 0:
            #             inside1 = edge.inside-1
            #     if not self.objects.isIn(cc_obj):


    def graph_matching(self, objGraph):
        #objGraph is the current object model (graph)
        # print objGraph.label
        go = self.check_number_cc(objGraph)
        if go:
            graph_cc = GraphCC()
            for cc in self.room.graph.get_indices():
                if not self.objects.isIn(cc):
                    graph_cc.add_node(cc)
            #--it first gets the largest CC that is calle mainNode
            mainNode = objGraph.getMaxNode()
            for cc in graph_cc.get_indices():
                if not self.objects.isIn(cc):
                    # print 'Object '+objGraph.label+': compare the main node '+str(mainNode.id)+' with node '+str(cc.id)
                    node_cond = self.check_node_attributes(mainNode, cc)
                    # check_node_attributes compares the nodes attributes (the histograms)
                    if node_cond:
                        foundObject = GraphCC()
                        imageGraph = GraphCC()
                        foundObject.add_node(mainNode)
                        imageGraph.add_node(cc)
                        check_max = objGraph.check_max_size(imageGraph, self.door_width, size=1)
                        if check_max:
                            # print 'find a matching for main node'
                            main_nodes = self.check_main_nodes_are_in(foundObject, objGraph)
                            found = False
                            if not main_nodes:
                                found = self.__matchAttributes(mainNode, cc, foundObject, imageGraph, objGraph, size=1)

                            else:
                                check_size = objGraph.check_size(imageGraph, self.door_width, size=1)
                                if check_size:
                                    found = True
                            if found:
                                # print 'found object: '+objGraph.label+' in room '+self.room.name
                                # print 'object completed'
                                imageGraph.label = objGraph.label
                                for cc in imageGraph.get_indices():
                                    self.objects.add_node(cc)
                                if 'noise' in imageGraph.label:
                                    # objGraph2 = objGraph
                                    # if not objGraph.extend:
                                    #     # objGraph2 = deepcopy(objGraph)
                                    objGraph.extend = True
                                    self.extend_noise(imageGraph, objGraph)
                                else:
                                    self.complete_object(imageGraph, objGraph)
                                objGraph.extend = False
                                # print 'with the following cc: '
                                for cc in imageGraph.get_indices():
                                    # print str(cc.id)
                                    self.objects.add_node(cc)
                                self.objects_matching.append(imageGraph)
                                self.search_from_group(imageGraph)



    def graph_matching2(self, objGraph, cc):
        go = self.check_number_cc(objGraph)
        imageGraph = GraphCC()
        imageGraph.add_node(cc)
        if go:
            mainNode = objGraph.getMaxNode()
            # print 'Oggetto '+objGraph.label+': confronto il main node '+str(mainNode.id)+' con il nodo '+str(cc.id)
            node_cond = self.check_node_attributes(mainNode, cc)
            if node_cond:
                check_max = objGraph.check_max_size(imageGraph, self.door_width, size=1)
                if check_max:
                    foundObject = GraphCC()
                    foundObject.add_node(mainNode)
                    main_nodes = self.check_main_nodes_are_in(foundObject, objGraph)
                    # print 'trovata somiglianza nodo main'
                    if not main_nodes:
                        found = self.__matchAttributes(mainNode, cc, foundObject, imageGraph, objGraph, size=1)
                        if found:
                            return True, imageGraph
                    else:
                        check_size = objGraph.check_size(imageGraph, self.door_width, size=1)
                        if check_size:
                            # print 'trovato un oggetto: '+objGraph.label+' nella stanza '+self.room.name
                            # print 'completo lo oggetto'
                            imageGraph.label = objGraph.label
                            self.complete_object(imageGraph, objGraph)
                            self.objects_matching.append(imageGraph)
                            for cc in imageGraph.get_indices():
                                self.objects.add_node(cc)
                            return True, imageGraph
        return False, imageGraph




    def __matchAttributes(self, node, cc_from, foundObject, imageGraph, objGraph, size):
        found = False
        list_edges1 = objGraph.get_edges_to(node)
        for edge in list_edges1:
            id1_a, id2_a = edge.get_nodes_id()
            inside1 = 0
            if id1_a == node.id:
                cc_obj = edge.node2
                inside1 = edge.inside
            else:
                cc_obj = edge.node1
                if edge.inside%2 == 1:
                    inside1 = edge.inside+1
                elif edge.inside != 0:
                    inside1 = edge.inside-1
            if not foundObject.isIn(cc_obj):
                list_edges2 = self.room.graph.get_edges_to(cc_from)
                for edge2 in list_edges2:
                    id1_b, id2_b = edge2.get_nodes_id()
                    inside2 = 0
                    if id1_b == cc_from.id:
                        cc_img = edge2.node2
                        inside2 = edge2.inside
                    else:
                        cc_img = edge2.node1
                        if edge2.inside%2 == 1:
                            inside2 = edge2.inside+1
                        elif edge.inside != 0:
                            inside2 = edge2.inside-1
                    isin_cond = False
                    if not imageGraph.isIn(cc_img) and not self.objects.isIn(cc_img) and not foundObject.isIn(cc_obj):
                        isin_cond = True
                    if isin_cond and edge2.type == edge.type and inside1 == inside2:
                        # print 'confronto la obj cc '+str(cc_obj.id)+' con la cc img '+str(cc_img.id)
                        # print 'con arco obj dalla '+str(node.id)+' e arco img dalla '+str(cc_from.id)
                        node_cond = self.check_node_attributes(cc_obj, cc_img)
                        direction = self.check_direction(node, cc_obj, cc_from, cc_img)
                        # print 'condizioni node, direction = '
                        # print str(node_cond)
                        # print str(direction)
                        if direction and node_cond:
                                    # print 'trovata somiglianza for interno'
                                    imageGraph.add_node(cc_img)
                                    foundObject.add_node(cc_obj)
                                    # print 'nodi obj trovati:'
                                    # for cc in foundObject.get_indices():
                                    #     print str(cc.id)
                                    # print 'nodi main ancora da trovare:'
                                    # for cc in objGraph.main_nodes:
                                    #     print str(cc.id)
                                    check_max = objGraph.check_max_size(imageGraph, self.door_width, size)
                                    # print 'check_max = '+str(check_max)
                                    if not check_max:
                                        imageGraph.remove_node(cc_img)
                                        foundObject.remove_node(cc_obj)
                                    else:
                                        check_size = objGraph.check_size(imageGraph, self.door_width, size)
                                        # print 'check_size = '+str(check_size)
                                        wall = self.check_wall_cond(imageGraph, objGraph.wall_cond)
                                        main_nodes_in = self.check_main_nodes_are_in(foundObject, objGraph)
                                        # print 'wall condition = '+str(wall)
                                        # print 'main nodes are in? '+str(main_nodes_in)
                                        if not wall or not main_nodes_in or not check_size:
                                            found = self.__matchAttributes(cc_obj, cc_img, foundObject, imageGraph, objGraph, size)
                                            # if found:
                                            #     return True
                                        else:
                                            found = True
                                            # return True
        return found

    def search_from_group(self, imgGraph):
        groups = self.get_group_from_obj(imgGraph.label)
        mainNode = imgGraph.getMaxNode()
        for group in groups:
            for i in range(1, len(group)):
                label = group[i][0:3]
                for obj in self.objects_graph:
                    if obj.label[0:3] == label:
                        for cc in self.room.graph.get_indices():
                            over_wall = imop.over_wall(self.room.graph.img_red_wall, cc, mainNode)
                            if not self.objects.isIn(cc) and not over_wall:
                                self.graph_matching2(obj, cc)



    def check_wall_cond(self, imageGraph, wall_cond):
        if wall_cond:
            for cc in imageGraph.get_indices():
                if self.room.check_wall_link(cc):
                    return True
            return False
        else:
            return True

    def complete_object(self, imageGraph, objGraph):
        graph = GraphCC()
        cc2 = CC(-1)
        cc2.x_min, cc2.x_max, cc2.y_min, cc2.y_max = imageGraph.get_extremes()
        for cc in imageGraph.get_indices():
            for edge in self.room.graph.get_edges_to(cc):
                id1, id2 = edge.get_nodes_id()
                if id1 == cc.id:
                    cc3 = edge.node2
                else:
                    cc3 = edge.node1
                inside = cc2.check_cc_inside_rectangle(cc3)
                if inside and not self.objects.isIn(cc3):
                    graph.add_node(cc3)
        for cc in graph.get_indices():
            imageGraph.add_node(cc)
        return imageGraph





    def size_cond(self, name, cc_img):
        if name[0:4] == 'sink':
            # print 'cc_img '+str(cc_img.id)+' rispetta la size_cond?'
            if cc_img.area <= 4.5*self.door_width**2:
                # print 'Yes!'
                return True
            else:
                # print 'No!'
                return False
        elif name[0:7] == 'crossed':
            area = (cc_img.width-2)*(cc_img.height-2)
            if 0.3 < cc_img.area/area < 0.7:
                return True
            else:
                return False
        else:
            return True

    def check_direction(self, cc_from1, cc_to1, cc_from2, cc_to2):
        ratio1 = cc_from1.area/cc_to1.area
        ratio2 = cc_from2.area/cc_to2.area
        if (ratio1 <= 1 and ratio2 <= 1) or (ratio1 > 1 and ratio2 > 1):
            # print 'same direction'
            ratio1 = min(cc_from1.area,cc_to1.area)/max(cc_from1.area,cc_to1.area)
            ratio2 = min(cc_from2.area,cc_to2.area)/max(cc_from2.area,cc_to2.area)
            if abs(ratio1-ratio2) <= self.areaThresh:
                return True
        elif abs(ratio1-ratio2) <= self.directionThresh:
            return True
        return False












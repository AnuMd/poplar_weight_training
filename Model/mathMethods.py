__author__ = 'pierpaolo'


import numpy as np
import math



def distance2D(ax,ay,bx,by):
        dist = np.sqrt(float((ax-bx)**2 + (ay-by)**2))
        return dist

def distance3D(a,b):
        dist = np.sqrt(float((a[0]-b[0])**2+(a[1]-b[1])**2+(a[2]-b[2])**2))
        return dist

    #pitagora find the angle in c with triangle c,b,a with ac segment horizontal from c to the vertical projection of b
    #
    #      b
    #     /|
    #    / |
    #   /  |
    #  /   |
    # c----a

def pitagora(cx, cy, bx, by, rad):
    if rad == True:
        if cy == by:
            if cx > bx:
                angle = 0.5 #normalized value of pi-greco   b------c
            else:
                angle = 0 #  c------b
        elif cx == bx:
            if cy < by:
                angle = 0.25
            else:
                angle = 0.75
        else:
            ipotenusa = distance2D(cx, cy, bx, by)
            cateto = distance2D(cx, cy, bx, cy)
            cosin = cateto/ipotenusa
            angle = (math.acos(cosin))/(2*math.pi)
            #evaluate the quarter
            if cx > bx and cy > by:   #second quarter...anticlockwise!
                angle = 0.5-angle
            elif cx > bx and cy < by: #third
                angle = 0.5+angle
            elif cx > bx and cy < by: #fourth
                angle = 1-angle
            #for the first quarter don't do anything!
    else:
        if cy == by:
            if cx > bx:
                angle = 180 #normalized value of pi-greco   b------c
            else:
                angle = 0 #  c------b
        elif cx == bx:
            if cy < by:
                angle = 90
            else:
                angle = 270
        else:
            ipotenusa = distance2D(cx, cy, bx, by)
            cateto = distance2D(cx, cy, bx, cy)
            cosin = cateto/ipotenusa
            angle = math.degrees(math.acos(cosin))
            #evaluate the quarter
            # if cx > bx and cy > by:   #second quarter...anticlockwise!
            #     angle = 0.5-angle
            # elif cx > bx and cy < by: #third
            #     angle = 0.5+angle
            # elif cx > bx and cy < by: #fourth
            #     angle = 1-angle
            # #for the first quarter don't do anything!
    return angle

def find_perpendicular(cnt, cx, cy, ax, ay):
    p = []
    dist = 0
    for xA in cnt:
        angle = carnot(ax,ay,xA[0][0],xA[0][1],cx,cy,rad=False)
        if angle == 90:
            p = (xA[0][0],xA[0][1])
            dist = distance2D(cx,cy,p[0],p[1])
            return p, dist
        elif 85 <= angle <= 95:
            p = (xA[0][0],xA[0][1])
            dist = distance2D(cx,cy,p[0],p[1])
    return p, dist

def calculate_angle(x1, y1, x2, y2, cx, cy):
        if y1 == y2 and x1 == x2:
            return 0
        angle = carnot(x1, y1, x2, y2, cx, cy, rad = False)

        return angle

    #Carnot's theorem to calculate the corner from the centroid to edges touching an other cc
def carnot(x_min,y_min,x_max,y_max,cx,cy,rad):
        ac = distance2D(x_min, y_min, cx, cy)
        bc = distance2D(x_max, y_max, cx, cy)
        ab = distance2D(x_max, y_max, x_min, y_min)

        #calculate the cosin of the corner in c using Carnot's Theorme
        if ac*bc != 0:
            cosin = ((-(ab)**2 + (ac)**2 + (bc)**2)/(2*ac*bc))
            #extract the corner and normalize
            if cosin > 1:
                ##print 'cosenu anomalu = ' + str(cosin)
                cosin = 1
            if cosin < -1:
                ##print 'cosenu anomalu = ' + str(cosin)
                cosin = -1
            # angle in normalized radiants
            if rad == True:
                corner = (math.acos(cosin))/(2*math.pi)
            # angle in degrees
            else:
                corner = math.degrees(math.acos(cosin))
        else:
            corner = 0
        return corner

def calculate_max_distance(cnt, cx, cy):
    max_distance = 0
    max_point = []
    for xA in cnt:
        dist = distance2D(cx, cy, xA[0][0], xA[0][1])
        if dist > max_distance:
            max_distance = dist
            max_point = (xA[0][0], xA[0][1])
    return max_distance, max_point

def calculate_min_distance(cnt, cx, cy):
    min_distance = 9999999
    min_point = []
    for xA in cnt:
        dist = distance2D(cx, cy, xA[0][0], xA[0][1])
        if dist < min_distance:
            min_distance = dist
            min_point = (xA[0][0], xA[0][1])
        # if cx == xA[0][0]:
        #     pointB = (xA[0][0], xA[0][1])
        #     distB = dist
        # if cy == xA[0][1]:
        #     pointA = (xA[0][0], xA[0][1])
        #     distA = dist
    return min_distance, min_point

def get_bottom_right(cnt):
    x_max = 0
    y_max = 0
    x_min = 999999
    y_min = 999999
    for xA in cnt:
        x_max = max(x_max, xA[0][0])
        y_max = max(y_max, xA[0][1])
        x_min = min(x_min, xA[0][0])
        y_min = min(y_min, xA[0][1])
    return x_max-x_min, y_max-y_min

def differenceQuozient(r1,r2,c1,c2):
    if c1 != c2:
        diff = float(r2-r1) / float(c1-c2)
    else:
        diff = 99999 * (r2-r1)
    return diff

def crossProduct(ar, br, cr, ac, bc, cc):
    v1 = [ac-cc, cr-ar]
    v2 = [bc-cc, cr-br]
    xp = v2[0]*v1[1] - v2[1]*v1[0]
    return xp
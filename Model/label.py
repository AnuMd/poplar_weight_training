__author__ = 'pierpaolo'

from connectedcomponent import CC

class Label(object):
    def __init__(self):
        self.name = 'None'
        self.cx = 0
        self.cy = 0
        self.x_min = 0
        self.y_min = 0
        self.x_max = 0
        self.y_max = 0


    def set_label(self, name, cx, cy, xmin, ymin, xmax, ymax):
        self.cx = cx
        self.cy = cy
        self.name = name
        self.x_min = xmin
        self.y_min = ymin
        self.x_max = xmax
        self.y_max = ymax

    def transform_to_cc(self):
        cc = CC(0)
        cc.x_min = self.x_min
        cc.y_min = self.y_min
        cc.x_max = self.x_max
        cc.y_max = self.y_max
        cc.center_x = self.cx
        cc.center_y = self.cy
        return cc


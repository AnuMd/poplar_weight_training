__author__ = 'stefano'

from side import Side
import numpy as np
import math
import cv2

class Rectangle():
    # def __init__(self):
    #     #
    #     #       _________l1________
    #     #      |                  |
    #     #   l4 |                  |l2
    #     #      |__________________|
    #     #                l3
    #     #
    #     #
    #     #this coordinate in cc
    #     self.x1=0
    #     self.y1=0
    #     self.x2=0
    #     self.y2=0
    #     self.x3=0
    #     self.y3=0
    #     self.x4=0
    #     self.y4=0
    #                 #  x1  y1  x2  y2
    #     self.skeleton=[ 0 , 0 , 0 , 0 ]
    #     self. image_skeleton=None
    #     self.id=-1
    #     self.num_set=0
    #     self.list_sides=[]
    #     self.list_points=[]
    #
    #     # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle 1, side of rectangle 2, point merge]
    #     self.list_adjacent_rect=[]
    #     #this specify the type of rectangle is wall, door, text, windows ....
    #     self.type=[]
    #
    #     # this coordinate in image
    #     self.x_min=0
    #     self.y_min=0
    #     self.x_max=0
    #     self.y_max=0
    #
    # #ATTRIBUTES NODE
    # #    self.id = id
    #     self._edges = []
    #     self._indices = {}
    #     #id dell'arco avra come valore nodo.id*100+len(self._list_edge)
    #     self._list_point = []
    #     self._type = ''
    #     self.image = None
    #     self._x_min = 0
    #     self._x_max = 0
    #     self._y_min = 0
    #     self._y_max = 0

    def __init__(self,id):
     # x1, y1
     # x_min,ymin_________l1________
        #      |                  |
        #   l4 |                  |l2
        #      |__________________|
        #                l3       x_max,ymax_
        #
        #
        #this coordinate in image
        self.x1=0
        self.y1=0
        self.x2=0
        self.y2=0
        self.x3=0
        self.y3=0
        self.x4=0
        self.y4=0
        # this coordinate in image
        self.x_min=0
        self.y_min=0
        self.x_max=0
        self.y_max=0
                    #  x1  y1  x2  y2
       # self.skeleton=[ 0 , 0 , 0 , 0 ]
        self.sk_x1 = 0
        self.sk_y1 = 0
        self.sk_x2 = 0
        self.sk_y2 = 0
        self.image_skeleton=None
        self.id=id
        self.num_set=0
        self.list_sides=[]
        self.list_points=[]

        # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle 1, side of rectangle 2, point merge]
        self.list_adjacent_rect=[]
        #this specify the type of rectangle is wall, door, text, windows ....
        self.type=""
        self.openig=[]
        self.oblique = False
        self.side_touch_wall=[]
        self.list_left_rect=[]
        self.list_right_rect=[]
        self.list_down_rect=[]
        self.list_up_rect=[]

    #ATTRIBUTES NODE
    #    self.id = id
        self._edges = []
        self._indices = {}
        #id dell'arco avra come valore nodo.id*100+len(self._list_edge)
        self._list_point = []
        # self._type = ''
        self.image = None
        self._x_min = 0
        self._x_max = 0
        self._y_min = 0
        self._y_max = 0

    def setData(self,r):
        self.x1 = r[0]
        self.y1 = r[1]
        self.x2 = r[2]
        self.y2 = r[3]
        self.x3 = r[4]
        self.y3 = r[5]
        self.x4 = r[6]
        self.y4 = r[7]
        self.x_min = self.x1
        self.y_min = self.y1
        self.x_max = self.x3
        self.y_max = self.y3
        self.set_skeleton()

    def get_max_len(self):

        d1 = np.sqrt((self.x1-self.x2)**2 + (self.y1-self.y2)**2)
        d2 = np.sqrt((self.x2-self.x3)**2 + (self.y2-self.y3)**2)
        if d1== 0 and d2 == 0:
            d1 = 1
        m = max(d1,d2)
        #m = max( self.x3 - self.x1, self.y3 - self.y1)+1
        return m

    def get_min_len(self):
        d1 = np.sqrt((self.x1-self.x2)**2 + (self.y1-self.y2)**2)
        d2 = np.sqrt((self.x2-self.x3)**2 + (self.y2-self.y3)**2)
        if d1== 0 and d2 == 0:
            m = 1
        else:
            m = min(d1,d2)
        return m
        #return min( self.x3 - self.x1 +1 , self.y3 - self.y1 +1 )

    def get_centroid(self):
        cx = float(self.x3+self.x1)/2
        cy = float(self.y3+self.y1)/2

        return cx,cy

    def get_distance_to_centroid(self,x,y):
        cx = float(self.x3+self.x1)/2
        cy = float(self.y3+self.y1)/2

        return np.sqrt(float(cx - x)**2 + float(cy - y)**2)

    def get_orientation(self):
        # ritorna le coordinate dei lati dell'anta
        if max( self.x3 - self.x1, self.y3 - self.y1) == (self.x3 - self.x1):

            ori = 'horizontal'
        else:
           # l1 = [self.x1,self.y1,self.x4,self.y4]
           # l2 = [self.x2,self.y2,self.x3,self.y3]
            ori = 'vertical'
        return ori

    def get_angular_coefficient(self):
        if not(self.x1==self.x2):
            m = float(self.y2 - self.y1)/float(self.x2 - self.x1)
            return m
        return np.inf


    def set_skeleton(self):


        x1_v=int((self.x1+self.x2)/2)
        y1_v=int((self.y1+self.y2)/2)
        x2_v=int((self.x3+self.x4)/2)
        y2_v=int((self.y3+self.y4)/2)

        x1_h=int((self.x1+self.x4)/2)
        y1_h=int((self.y1+self.y4)/2)
        x2_h=int((self.x3+self.x2)/2)
        y2_h=int((self.y3+self.y2)/2)

        d_h=self.distance_by_two_point(x1_h,y1_h,x2_h,y2_h)
        d_v=self.distance_by_two_point(x1_v,y1_v,x2_v,y2_v)

        if d_h>d_v:

            self.sk_x1 = x1_h
            self.sk_y1 = y1_h
            self.sk_x2 = x2_h
            self.sk_y2 = y2_h
            # creation of the image skeleton
            # self.image_skeleton=np.zeros((d_v+1,d_h+1),dtype=np.uint8)
            # x1=int(np.abs(self.x1-self.x4)/2)
            # y1=int(np.abs(self.y1-self.y4)/2)
            # x2=int(np.abs(self.x3-self.x2)/2)
            # y2=int(np.abs(self.y3-self.y2)/2)
            # cv2.line( self.image_skeleton,(x1,y1),(x2,y2),255,1)

        else:
            self.sk_x1 = x1_v
            self.sk_y1 = y1_v
            self.sk_x2 = x2_v
            self.sk_y2 = y2_v
            # self.image_skeleton=np.zeros((d_v+1,d_h+1),dtype=np.uint8)
            # x 1=int(np.abs(self.x1-self.x2)/2)
            # y1=int(np.abs(self.y1-self.y2)/2)
            # x2=int(np.abs(self.x3-self.x4)/2)
            # y2=int(np.abs(self.y3-self.y4)/2)
            # cv2.line( self.image_skeleton,(x1,y1),(x2,y2),255,1)

        # si controlla se il rettangolo si sviluppa verticalmente oppure orizzontalmente, a questo punto si crea immagine va bene anche per i rettangoli obliqui


    def set_sides(self):
        s1=Side()
        s2=Side()
        s3=Side()
        s4=Side()
        #first side
        s1.x1=self.x1
        s1.y1=self.y1
        s1.x2=self.x2
        s1.y2=self.y2
        s1.numSide=1
        #second side
        s2.x1=self.x2
        s2.y1=self.y2
        s2.x2=self.x3
        s2.y2=self.y3
        s2.numSide=2
        #third side
        s3.x1=self.x3
        s3.y1=self.y3
        s3.x2=self.x4
        s3.y2=self.y4
        s3.numSide=3
        #fourth side
        s4.x1=self.x4
        s4.y1=self.y4
        s4.x2=self.x1
        s4.y2=self.y1
        s4.numSide=4
        self.list_sides.append(s1)
        self.list_sides.append(s2)
        self.list_sides.append(s3)
        self.list_sides.append(s4)
        self.set_skeleton()

    def get_minimum_distance_from_the_point(self,x,y):
        # input: coordinate x and y of a point P
        # output: minimun distance of P from (x1,y1), (x2,y2),(x3,y3),(x4,y4)

        #d1 distance of point x,y from l1 side
        if(x >= self.x1 and x <= self.x2):
            d1 = np.abs(y - self.y1)
        elif(x < self.x1):
            d1 = math.sqrt(float(x - self.x1)**2 + float(y - self.y1)**2)
        elif(x > self.x2):
            d1 = math.sqrt(float(x - self.x2)**2 + float(y - self.y2)**2)

        #d2 distance of point x,y from l2 side
        if(y >= self.y2 and y <= self.y3):
            d2 = np.abs(x - self.x2)
        elif(y < self.y2):
            d2 = math.sqrt(float(x - self.x2)**2 + float(y - self.y2)**2)
        elif(y > self.y3):
            d2 = math.sqrt(float(x - self.x3)**2 + float(y - self.y3)**2)

        #d3 distance of point x,y from l3 side
        if(x >= self.x4 and x <= self.x3):
            d3 = np.abs(y - self.y4)
        elif(x < self.x4):
            d3 = math.sqrt(float(x - self.x4)**2 + float(y - self.y4)**2)
        elif(x > self.x3):
            d3 = math.sqrt(float(x - self.x3)**2 + float(y - self.y3)**2)

        #d2 distance of point x,y from l2 side
        if(y >= self.y1 and y <= self.y4):
            d4 = np.abs(x - self.x1)
        elif(y < self.y1):
            d4 = math.sqrt(float(x - self.x1)**2 + float(y - self.y1)**2)
        elif(y > self.y4):
            d4 = math.sqrt(float(x - self.x4)**2 + float(y - self.y4)**2)

        # d1 = math.sqrt(float(x - self.x1)**2 + float(y - self.y1)**2)
        # d2 = math.sqrt(float(x - self.x2)**2 + float(y - self.y2)**2)
        # d3 = math.sqrt(float(x - self.x3)**2 + float(y - self.y3)**2)
        # d4 = math.sqrt(float(x - self.x4)**2 + float(y - self.y4)**2)
        return min(d1,d2,d3,d4)

    def distance_by_two_point( self,x1, y1, x2, y2):

          return math.sqrt(float(x2 - x1)**2 + float(y2 - y1)**2)

    def get_area(self):
        return((self.x2 - self.x1 )+1) *( ( self.y3 - self.y2 )+1)



    # this method are use in the graph


    def get_edge(self, a):
        return self._edges[self._indices[a]]

    def add_edge(self, a):
        if a not in self._indices:
            self._edges.append(a)
            self._indices[a] = len(self._edges) -1
        return self.get_edge(a)

    def edges(self):
        return self._edges[:]

    def remove_edge(self,a):
        if a in self._indices:
            self._edges.remove(a)
            for e in self._edges[self._indices[a]:len(self._edges)]:
                self._indices[e] = self._indices[e] - 1
            del self._indices[a]



    # ATTRIBUTI
    def set_type(self, t):
        self._type = t

    def get_type(self):
        return  self._type

    # METODI COORDINATE
    def set_coordinate(self,x1,y1,x2,y2):

        self._x_min=x1
        self._y_min=y1
        self._x_max=x2
        self._y_max=y2


    def get_coordinate(self):

        return self._x_min, self._y_min, self._x_max, self._y_max


    # METODI IMMAGINE
    def set_image(self,i):
        self._image = i


    def get_image(self):
        return self._image


    # METODI LIST_POINT
    def set_list_point(self,l):
        self._list_point.extend(l)


    def get_list_point(self):
        return self._list_point[:]


    def exetend_list_point(self,l):
        self._list_point.append(l)





    # redefines the operator ==
    def __eq__(self, x):
        return x.id == self.id

    # redefines the operator !=
    def __ne__(self, x):
        return not (x == self)

    # redefines the operator "in" for set
    def __hash__(self):
        return self.id.__hash__()

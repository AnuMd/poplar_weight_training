__author__ = 'anu'

import cv2,random, os,math
import numpy as np
from operator import itemgetter
from numpy import array
from natsort import natsorted

from open_plan_line import line


class planClass:
    def __init__(self,current_directory,output_directory, debug_path, image,image_name,inner_cont):
        if inner_cont:
            self.image = image
            self.image_name=image_name
            #--non-test path
            self.orginal_image_path = output_directory+self.image_name+'/no_text.png'
            #---test path
            # self.orginal_image_path = current_directory+'/input_fps/POPLAR/'+self.image_name+'.png'
            # print 'self.orginal_image_path',self.orginal_image_path
            self.gray_image = cv2.imread(self.orginal_image_path,0)

            self.height,self.width = image.shape
            self.output_directory = debug_path
        else:
            self.height,self.width = image.shape


    def find_rooms(self,debug_mode):
        gray = self.gray_image
        if debug_mode:
            cv2.imwrite(self.output_directory+'gray.png',gray)
        ret,thresh = cv2.threshold(gray,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)

        #------create 'new_cont_array' a 2D array with [[contour values],[contour_area]]
        contour_and_area = []
        for idx, cnt in enumerate(contours):
            contour_and_area.append([])
            contour_and_area[idx].append([cnt])
            contour_and_area[idx].append([cv2.contourArea(cnt)])

        #------sort array by descending area values
        contour_and_area.sort(key=itemgetter(1),reverse=True)
        final_cont_array = contour_and_area[0][0]
        #-----------------------------------------------------------------------------------------------
        #-------room check and generation
        #-----create array of random color values
        color = [[0,0,255],[0,255,0],[255,0,0],[255,255,0],[0,150,0],[150,0,255],[128,0,255],[0,128,255],[0,0,0]]
        #-----create balnk image to write rooms and blank file to write room details and make background white
        all_rooms = ~(np.zeros((self.height,self.width,3), np.uint8))
        bw_rooms = ~(np.zeros((self.height,self.width,3), np.uint8))

        #------getting cordinates from contour
        room_count =0
        for cn_num, cnt in enumerate(final_cont_array):
            color_val = tuple(random.choice(color))
            #------draw all room contours to single image
            cv2.drawContours(all_rooms,[cnt],-1,color_val,5)
            cv2.drawContours(bw_rooms,[cnt],-1,(0,0,0),5)

            #----- write each room contour to new image
            empty_new_image = ~(np.zeros((self.height,self.width,3), np.uint8))
            cv2.drawContours(empty_new_image,[cnt],-1,(0,0,0),1)
            cv2.imwrite(self.output_directory+'All_Rooms/'+str(room_count)+'.png',empty_new_image)

        #-------write all room contours
        cv2.imwrite(self.output_directory+"All_Rooms_in_Color.png",all_rooms)
        cv2.imwrite(self.output_directory+"All_Rooms_in_BW.png",bw_rooms)


    def find_open_plans(self,text_cordinates,debug_mode,avg_door_width,all_contour_lines,path_gravvitas):
        rooms_path = self.output_directory+'All_Rooms/'
        rooms_list_unsorted = os.listdir(rooms_path)
        # rooms_list_sorted = rooms_list_unsorted.sort()
        #--sort t get fp_1.png, fp_2.png instead of fp_1.png,fp_11.png,fp_2.png----NATURAL SORTING
        rooms_list = natsorted(rooms_list_unsorted)
        image_allrooms_color = cv2.imread(self.output_directory+"All_Rooms_in_Color.png",cv2.IMREAD_COLOR)

        #--data to create gravvitas file
        room_details_to_write = []
        text_num = 0
        text_data =[]
        for contourID, room in enumerate(rooms_list):
            if debug_mode:
                image_to_write = cv2.imread(rooms_path+room,cv2.IMREAD_COLOR)

            gray_room = cv2.imread(rooms_path+room,cv2.IMREAD_GRAYSCALE)
            ret,thresh = cv2.threshold(gray_room,0,255,1)
            contours,hierachy = cv2.findContours(thresh,1,2)
            temp_text =[]
            #--data to create gravvitas file
            temp_room_details_to_write= []
            for count, cnt in enumerate(contours):
                if count==1:
                    #--data to create gravvitas file
                    temp_room_details_to_write.append(cnt)
                    if debug_mode:
                        cv2.drawContours(image_to_write,[cnt],-1,(200,0,0),-1)
                    for i, elem in enumerate(text_cordinates):
                        # print elem
                        x1,y1 = elem [1]
                        #check if the text is within a contour
                        dist = cv2.pointPolygonTest(cnt,(x1,y1),False)
                        if dist ==1:
                            text_num=text_num+1
                            temp_text.append(elem)
                            #--data to create gravvitas file
                            temp_room_details_to_write.append([elem[0],elem[1]])
                            # temp_room_details_to_write.append([elem[0],elem[2],elem[3],elem[4],elem[5],elem[6]])
                            if debug_mode:
                                # cv2.circle(image_to_write, (x1, y1), 10, (0, 0, 255), -1)
                                cv2.putText(image_to_write,str(elem[0]),(x1,y1),cv2.FONT_HERSHEY_PLAIN, 5, (0, 0, 255), 5)

            #--data to create gravvitas file
            room_details_to_write.append(temp_room_details_to_write)
            text_data.append([contourID,temp_text])
            if debug_mode:
                cv2.imwrite(self.output_directory+'All_Rooms/TEXT_'+str(contourID)+'.png',image_to_write)
            for row in text_cordinates:
                # print row
                cv2.circle(image_allrooms_color, (row[1][0], row[1][1]), 10, (0, 0, 255), -1)

            cv2.imwrite(self.output_directory+"All_Rooms_Text_Labels.png",image_allrooms_color)

        for elem in text_data:
            contID= elem[0]
            if len(elem[1])>1:
                open_area = cv2.imread(self.output_directory+'All_Rooms/'+str(contID)+'.png',cv2.IMREAD_GRAYSCALE)
                contour_image,all_contour_lines = self.improve_contours(open_area,avg_door_width,contID,all_contour_lines,[])
                cv2.imwrite(self.output_directory+'OpenPlans/Open_plan_'+str(contID)+'.png',contour_image)

        #--write text to gravvitas
        no_of_rooms = 0
        room_details_file = open(path_gravvitas+self.image_name+'_1_room_details.txt', 'w')
        room_details_file.write('Total No of Rooms: '+str(len(room_details_to_write))+'\n')
        for row_count,room_row in enumerate(room_details_to_write):
            if len(room_row)>0:
                if len(room_row[1:])<=1:
                    contour_data = ''
                    for c,cont_point in enumerate(room_row[0]):
                        if c==0:
                            contour_data  = contour_data+'['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                        else:
                            contour_data  = contour_data+',['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                    no_of_rooms += 1
                    room_details_file.write('single : room '+str(no_of_rooms)+' : ')
                    if len(room_row[1:])>0:
                        for text_details in room_row[1:]:
                            #--get cordinates
                            x1,y1 = text_details[1]
                            #--write text: cordinates
                            room_details_file.write(str(text_details[0])+' : ['+str(x1)+','+ str(y1)+'] : ')
                            # #---get 'confidence,dict,orginal_word,difference'
                            # confidence,dict,original_word,difference = str(text_details[2]),str(text_details[3]),str(text_details[4]),str(text_details[5])
                            # if len(original_word)==0:
                            #     original_word = 'None'
                            # room_details_file.write('confidence,dict,orginal_word,difference : ['+confidence+','+dict+','+original_word+','+difference+'] : ')
                    else:
                        room_details_file.write('0 : 0 : ')
                    room_details_file.write('contour_details : ')
                    room_details_file.write(contour_data+'\n')
                    room_details_file.write('\n')

        room_details_file.close()


        return all_contour_lines,text_data,no_of_rooms


    def improve_contours(self,open_plan_image,avg_door_width,contID,all_contour_lines,conts):
        #define threshold values for
        # 1. minimum possible line length to have
        min_line_length = int(avg_door_width/20)-2
        # 2.minimum possible x and y differences(used to straighten lines)
        min_x_difference = int(avg_door_width/15)-2
        min_y_difference = int(avg_door_width/15)-2
        contour_points  = []
        ret,thresh = cv2.threshold(open_plan_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        if len(conts)==0:
            for count, current_cont in enumerate(contours):
                if count==1:
                    for l, line_level1 in enumerate(current_cont):
                        for line_level2 in line_level1:
                            contour_points.append(line_level2)
                            break
        else:
            for l, line_level1 in enumerate(conts):
                for line_level2 in line_level1:
                    contour_points.append(line_level2)
                    break

        cont_lines_to_pre_process,first_element = [],0
        for l,cordinate in enumerate(contour_points):
            x,y = cordinate
            # print x,y,l
            if l==0:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                first_element=[x,y]
            elif l==len(contour_points)-1:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                cont_lines_to_pre_process[l-1].append([x,y])
                cont_lines_to_pre_process[l].append(first_element)
            else:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                cont_lines_to_pre_process[l-1].append([x,y])

        # if len(cont_lines_to_pre_process) != 47:
        #     print 'len(cont_lines_to_pre_process)',len(cont_lines_to_pre_process)
        #     test_path = os.getcwd()+'_'+str(contID)+'_TEST.png'
        #     print 'TEST path',test_path
        #     test_image = ~(np.zeros((1500, 2000, 3), np.uint8))
        #     for test_line in cont_lines_to_pre_process:
        #         print 'test_line',test_line
        #         cv2.line(test_image, (tuple(test_line[0])), (tuple(test_line[1])), (0, 0, 255), 2,
        #              cv2.cv.CV_AA)
        #     cv2.imwrite(test_path, test_image)

        cont_points_after_pre_process = []
        discarded_lines_total_length = 0
        for l,each_line in enumerate(cont_lines_to_pre_process):
            x1,y1 = each_line[0]
            x2,y2 = each_line[1]
            line_length = math.hypot(x2 - x1, y2 - y1)
            # print each_line,line_length,'discarded_lines_total_length-',discarded_lines_total_length
            if l ==0:
                cont_points_after_pre_process.append(each_line[0])
                # if len(cont_lines_to_pre_process) != 47:
                #     print '1st Appended', each_line[0]
                if line_length<=min_line_length:
                    # if len(cont_lines_to_pre_process) != 47:
                    #     print line_length, 'length is <',min_line_length
                    discarded_lines_total_length = discarded_lines_total_length+line_length
                    continue
                else:
                    # if len(cont_lines_to_pre_process) != 47:
                    #     print line_length, 'length is >', min_line_length
                    cont_points_after_pre_process.append(each_line[1])
                    # if len(cont_lines_to_pre_process) != 47:
                    #     print '1st Appended', each_line[1]
            elif l== len(cont_lines_to_pre_process)-1:
                # earlier_discarded_length = 0
                # print 'discarded_lines_total_length',discarded_lines_total_length
                if line_length<=min_line_length:
                    if discarded_lines_total_length<=min_line_length:
                        # if len(cont_lines_to_pre_process) != 47:
                        #     print 'Deleted', cont_points_after_pre_process[-1]
                        del cont_points_after_pre_process[-1]
                        # earlier_discarded_length = discarded_lines_total_length
                        # discarded_lines_total_length = discarded_lines_total_length+line_length


                else:
                    # if earlier_discarded_length> min_line_length:
                    cont_points_after_pre_process.append(each_line[0])
                    # if len(cont_lines_to_pre_process) != 47:
                    #     print 'Last Appended', each_line[0]
                # if earlier_discarded_length > min_line_length:
                #     cont_points_after_pre_process.append(each_line[0])

                cont_points_after_pre_process.append(each_line[1])
                # if len(cont_lines_to_pre_process) != 47:
                #     print 'Last Appended', each_line[1]
            else:
                # if len(cont_lines_to_pre_process) != 47 and (l==4 or l==5 or l==6):
                #     print l, each_line
                if line_length<=min_line_length:
                    # if len(cont_lines_to_pre_process) != 47 and (l==4 or l==5 or l==6):
                    #     print line_length, 'length is <', min_line_length
                    discarded_lines_total_length = discarded_lines_total_length+line_length
                    # discarded_lines_total_length = line_length
                else:
                    if discarded_lines_total_length > min_line_length:
                        # if len(cont_lines_to_pre_process) != 47 and (l==4 or l==5 or l==6):
                        #     print 'earlier lines were shorter', discarded_lines_total_length, '>', min_line_length
                        cont_points_after_pre_process.append(each_line[0])
                        cont_points_after_pre_process.append(each_line[1])
                        # if len(cont_lines_to_pre_process) != 47:
                        #     print 'Appended', each_line[0], each_line[1]
                    else:
                        # if len(cont_lines_to_pre_process) != 47 and (l==4 or l==5 or l==6):
                        #     print 'earlier line was fully inserted', discarded_lines_total_length, '<', min_line_length
                        #     print 'Other Inserted ', line_length, each_line[1], discarded_lines_total_length
                        cont_points_after_pre_process.append(each_line[1])
                        discarded_lines_total_length = 0
                        # if len(cont_lines_to_pre_process) != 47:
                        #     print 'Appended', each_line[1]

        # if len(cont_lines_to_pre_process) != 47:
        #     print '---------------cont_points_after_pre_process----------------------'
        #     for each_line in cont_points_after_pre_process:
        #         print 'each_line',each_line

        final_contour_points,i_point,last_point = [],0,0
        line_obj = line()
        # contour_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        line_gradient_x , line_gradient_y = 0,0
        line_gradient_empty = False
        for i,current_point in enumerate(cont_points_after_pre_process):
            x1, y1 = cont_points_after_pre_process[i-1]
            x2,y2 = current_point
            # print 'current_point',current_point
            if i==0:
                final_contour_points.append(current_point)
                # print '1ST append',current_point
            elif i==len(cont_points_after_pre_process)-1:
                final_contour_points.append(current_point)
                # print 'LAST append',current_point
            else:
                if i==1 or line_gradient_empty:
                    current_line_length = math.hypot(x2 - x1, y2 - y1)
                    if current_line_length>min_line_length:
                        current_gradient_x,current_gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
                        line_gradient_x, line_gradient_y = current_gradient_x,current_gradient_y
                        line_gradient_empty = False
                    else:
                        line_gradient_empty= True
                        continue

                x3,y3 = cont_points_after_pre_process[i+1]
                next_gradient_x,next_gradient_y = line_obj.find_line_gradient(x2,y2,x3,y3)
                x_difference = abs(line_gradient_x-next_gradient_x)
                y_difference = abs(line_gradient_y-next_gradient_y)
                #--check if gradients are different
                if ~(x_difference < 0.05 and y_difference< 0.05):
                    next_line_length = math.hypot(x3 - x2, y3 - y2)
                    if next_line_length>min_line_length:
                        # print 'NORMAL append ',current_point
                        final_contour_points.append(current_point)
                        line_gradient_x, line_gradient_y = next_gradient_x,next_gradient_y
        # if contID==0:
        #     print '-----------final_contour_points-----------------------------------------------------------------------'
        #     for row in final_contour_points:
        #         print row
        #---to straighten lines that have long length but 1px differences
        new_contour_points  = []
        for p, current_point in enumerate(final_contour_points):
            point_changed = False
            if p == 0:
                new_contour_points.append(current_point)
            else:
                current_x, current_y = current_point
                last_x, last_y = new_contour_points[-1]
                x_difference = abs(last_x-current_x)
                y_difference = abs(last_y-current_y)
                if x_difference<min_x_difference:
                    new_contour_points.append([last_x,current_y])
                    point_changed= True
                elif y_difference<min_y_difference:
                    new_contour_points.append([current_x,last_y])
                    point_changed= True
                else:
                    new_contour_points.append(current_point)

                if p == len(final_contour_points)-1:
                    # if contID==9:
                    #     print 'BEFORE Loop'
                    #     for row in new_contour_points:
                    #         print row
                    first_element_changed = False
                    if point_changed:
                        new_contour_points[0]=new_contour_points[-1]
                        first_element_changed = True
                    if first_element_changed:
                        last_element = 0
                        # if contID==9:
                        #     print 'test 1--'
                        #     for row in new_contour_points:
                        #         print row
                        for p1,current_point in enumerate(new_contour_points):
                            if p1>0:
                                # print '+++++',current_point
                                current_x, current_y = current_point
                                last_x, last_y = new_contour_points[last_element]
                                x_difference = abs(last_x-current_x)
                                y_difference = abs(last_y-current_y)
                                if x_difference<min_x_difference:
                                    new_contour_points[p1]=[last_x,current_y]
                                    last_element += 1
                                elif y_difference<min_y_difference:
                                    new_contour_points[p1]=[current_x,last_y]
                                    last_element += 1
                                else:
                                    # if contID==9:
                                    #     print 'BREAK from', current_point
                                    break
                                # if contID==9:
                                #     print 'test RRRRR--'
                                #     for row in new_contour_points:
                                #         print row



        #---check if some lines are basically the same line broken to segments
        # new_contour_points = final_contour_points
        image_contour_lines,first_element = [],0
        for l,cordinate in enumerate(new_contour_points):
            x,y = cordinate
            if l==0:
                image_contour_lines.append([])
                image_contour_lines[l].append([x,y])
                first_element=[x,y]
            elif l==len(final_contour_points)-1:
                image_contour_lines[l-1].append([x,y])
                if ~(x==first_element[0] and y==first_element[1]):
                    image_contour_lines.append([])
                    image_contour_lines[l].append([x,y])
                    image_contour_lines[l].append(first_element)
            else:
                image_contour_lines.append([])
                image_contour_lines[l].append([x,y])
                image_contour_lines[l-1].append([x,y])
                # last_element = [x,y]

        #--if starting line and end line has same gradient, connect them as one line
        line_number = 0
        while line_number<len(image_contour_lines)-1:
            line1_start, line1_end = image_contour_lines[line_number]
            line2_start, line2_end = image_contour_lines[line_number+1]
            x1,y1 = line1_start
            x2,y2 = line1_end
            x3,y3 = line2_start
            x4,y4 = line2_end
            line1_gradient_x,line1_gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
            line2_gradient_x,line2_gradient_y = line_obj.find_line_gradient(x3,y3,x4,y4)
            x_difference = abs(line1_gradient_x-line2_gradient_x)
            y_difference = abs(line1_gradient_y-line2_gradient_y)
            if x_difference < 0.05 and y_difference< 0.05:
                image_contour_lines[line_number] = [line1_start,line2_end]
                del image_contour_lines[line_number+1]
                line_number -= 1
            else:
                line_number += 1

        #--- special case for first and last elements
        line1_start, line1_end = image_contour_lines[0]
        x1,y1 = line1_start
        x2,y2 = line1_end
        line1_gradient_x,line1_gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
        line2_start, line2_end = image_contour_lines[-1]
        x3,y3 = line2_start
        x4,y4 = line2_end
        line2_gradient_x,line2_gradient_y = line_obj.find_line_gradient(x3,y3,x4,y4)
        x_difference = abs(line1_gradient_x-line2_gradient_x)
        y_difference = abs(line1_gradient_y-line2_gradient_y)
        if x_difference < 0.05 and y_difference< 0.05:
            del image_contour_lines[0]
            image_contour_lines[-1] = [line2_start,line1_end]


        contour_points_final = []
        for row_num,each_line in enumerate(image_contour_lines):
            contour_points_final.append([each_line[0]])

        contour_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        for l,each_line in enumerate(image_contour_lines):
            cv2.line(contour_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)

        contour_points_final_to_insert = array(contour_points_final)

        all_contour_lines.append([contID])
        all_contour_lines[len(all_contour_lines)-1].append(image_contour_lines)
        all_contour_lines[len(all_contour_lines)-1].append(contour_points_final_to_insert)

        return contour_image,all_contour_lines


    def extending_edges(self,avg_door_width,name, image_name,image_path,current_open_plan_contour_data,open_plan_text_cordinate,debug_mode):
        original_edges_and_extensions = []
        open_plan_image = cv2.imread(image_path,cv2.IMREAD_GRAYSCALE)
        openplan_height,openplan_width = open_plan_image.shape

        #------ get lines from cordinate_list==lines of contour
        image_contour_lines=current_open_plan_contour_data[1]
        contour_to_check = current_open_plan_contour_data[2]
        empty_new_image = ~(np.zeros((openplan_height,openplan_width,3), np.uint8))
        for each_line in image_contour_lines:
            cv2.line(empty_new_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)
        cv2.imwrite(self.output_directory+name+'/Edge_Extension_Lines/'+name+'_STEP_01.png',empty_new_image)


        count = 0
        min_line_length  = avg_door_width / 5
        #line extraction and finding intersections
        for l1, level1 in enumerate(image_contour_lines):
            x1,y1 = level1[0]
            x2,y2 = level1[1]
            #--check if length of current line is not too small
            line_length = math.hypot(x2 - x1, y2 - y1)
            #--change on 06/03/2017
            # if line_length>avg_door_width/40:
            if line_length > min_line_length:
                #----find if two points beside line are inside contour
                line_obj1 = line()
                point_inside_line = line_obj1.find_points_next_to_end_points_in_line(x1,y1,x2,y2,line_length,contour_to_check)
                if point_inside_line:
                    #--find line numbers to find before and after lines
                    line_number_before, line_number_after = 0,0
                    if l1==0:
                        line_number_before = len(image_contour_lines)-1
                    elif l1== len(image_contour_lines)-1:
                        line_number_after = 0
                    else:
                        line_number_before = l1-1
                        line_number_after = l1+1
                    #--find length of line before current line
                    line_before = image_contour_lines[line_number_before]
                    before_x1,before_y1 = line_before[0]
                    before_x2,before_y2 = line_before[1]
                    length_line_before = math.hypot(before_x2 - before_x1, before_y2 - before_y1)
                    #--find length of line after current line
                    line_after = image_contour_lines[line_number_after]
                    after_x1,after_y1 = line_after[0]
                    after_x2,after_y2 = line_after[1]
                    length_line_after = math.hypot(after_x2 - after_x1, after_y2 - after_y1)
                    #--if line before or after are more than door width then: extend that line
                    if length_line_before>int(min_line_length) or length_line_after>int(min_line_length):
                        for l2, level2 in enumerate(image_contour_lines):
                            if l1==0:
                                lower = len(image_contour_lines)-1
                            else:
                                lower = l1-1
                            if l1==len(image_contour_lines)-1:
                                upper = 0
                            else:
                                upper = l1+1
                            check_value = False
                            if l2==l1 or l2==upper or l2==lower:
                                check_value = True
                            if check_value == False:
                                x3,y3 = level2[0]
                                x4,y4 = level2[1]
                                max_x1 = max(x3,x4)
                                min_x1 = min(x3,x4)
                                max_y1 = max(y3,y4)
                                min_y1 = min(y3,y4)
                                line_obj = line()
                                intersect_x,intersect_y = line_obj.find_contour_intersections(x1,y1,x2,y2,x3,y3,x4,y4)
                                if (intersect_x>=min_x1 and intersect_x<=max_x1 and intersect_y>=min_y1 and intersect_y<=max_y1):
                                    distance_to_first_cordinate_line1 = math.hypot(intersect_x - x1, intersect_y - y1)
                                    distance_to_second_cordinate_line1 = math.hypot(intersect_x - x2, intersect_y - y2)
                                    closest_point_to_intersection = min(distance_to_first_cordinate_line1,distance_to_second_cordinate_line1)
                                    extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y = 0,0,0,0
                                    if closest_point_to_intersection==distance_to_first_cordinate_line1:
                                       extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y = x1,y1,intersect_x,intersect_y
                                    if closest_point_to_intersection==distance_to_second_cordinate_line1:
                                        extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y = x2,y2,intersect_x,intersect_y
                                    max_e_x = max(extended_edge_p1_x,extended_edge_p2_x)
                                    min_e_x = min(extended_edge_p1_x,extended_edge_p2_x)
                                    max_e_y = max(extended_edge_p1_y,extended_edge_p2_y)
                                    min_e_y = min(extended_edge_p1_y,extended_edge_p2_y)
                                    text_intersects_flag=False
                                    line_obj=line()
                                    # -----check if extended_line intersects with the text boundary box
                                    for t,text_data in enumerate(current_open_plan_contour_data[3:]):
                                        for bb_line in text_data[3]:
                                            bb_line_x1,bb_line_y1 = bb_line[0]
                                            bb_line_x2,bb_line_y2 = bb_line[1]
                                            bb_line_max_x = max(bb_line_x1,bb_line_x2)
                                            bb_line_min_x = min(bb_line_x1,bb_line_x2)
                                            bb_line_max_y = max(bb_line_y1,bb_line_y2)
                                            bb_line_min_y = min(bb_line_y1,bb_line_y2)
                                            intersect_x1, intersect_y1 = line_obj.find_contour_intersections(extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y,bb_line_x1,bb_line_y1,bb_line_x2,bb_line_y2)
                                            if (intersect_x1>=min_e_x and intersect_x1<=max_e_x and intersect_y1>=min_e_y and intersect_y1<=max_e_y) and (intersect_x1>=bb_line_min_x and intersect_x1<=bb_line_max_x and intersect_y1>=bb_line_min_y and intersect_y1<=bb_line_max_y):
                                                text_intersects_flag= True
                                                break
                                        if text_intersects_flag:
                                            break
                                    if text_intersects_flag==False:
                                        # --changed on 19/03/2017 - inside check_line_inside_contour
                                        num_of_points_outside_contour = line_obj.check_line_inside_contour(extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y,contour_to_check)
                                        if num_of_points_outside_contour==0:
                                            ee_x1,ee_y1 = int(extended_edge_p1_x),int(extended_edge_p1_y)
                                            ee_x2,ee_y2 = int(extended_edge_p2_x),int(extended_edge_p2_y)

                                            original_edges_and_extensions.append([[[x1,y1],[x2,y2]],
                                                                                  [[ee_x1,ee_y1], [ee_x2,ee_y2]]])
                                            if debug_mode:
                                                cv2.line(empty_new_image,
                                                         (x1,y1),
                                                         (x2,y2),
                                                         (0, 0, 255), 2, cv2.CV_AA)
                                                cv2.circle(empty_new_image,(intersect_x,intersect_y),5, (255, 255, 0), -1)
                                                cv2.line(empty_new_image,(int(extended_edge_p1_x),int(extended_edge_p1_y)),(int(extended_edge_p2_x),int(extended_edge_p2_y)),(0,255,0),2,cv2.CV_AA)

                                            count = count+1
        if debug_mode:
            cv2.imwrite(self.output_directory+name+'/Edge_Extension_Lines/'+name+'_STEP_03.png',empty_new_image)


            extended_line_sections_image = cv2.imread(self.output_directory+name+'/Edge_Extension_Lines/'+name+'_STEP_01.png',cv2.IMREAD_COLOR)
            # extended_line_sections_image = ~(np.zeros((openplan_height,openplan_width,3), np.uint8))
            for ee_row in original_edges_and_extensions:
                extension = ee_row[1]
                x1,y1 = extension[0]
                x2,y2 = extension[1]
                cv2.line(extended_line_sections_image,(x1,y1),(x2,y2),(0,255,0),2,cv2.CV_AA)
            for cordinate in open_plan_text_cordinate:
                cv2.circle(extended_line_sections_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
            cv2.imwrite(self.output_directory+name+'/Edge_Extension_Lines/'+name+'_STEP_04.png',extended_line_sections_image)


        return original_edges_and_extensions



    def improve_outer_contour(self,open_plan_image,avg_door_width,outermost_cont):
        contour_points  = []
        ret,thresh = cv2.threshold(open_plan_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        for count, current_cont in enumerate(contours):
            if outermost_cont:
                img_height, img_width = open_plan_image.shape
                img_size = img_height * img_width
                contour_size = cv2.contourArea(current_cont)
                contour_ratio = contour_size/img_size
                if contour_ratio > 0.2 and contour_ratio < 0.9:
                # if count==len(contours)-2:
                    for l, line_level1 in enumerate(current_cont):
                        for line_level2 in line_level1:
                            contour_points.append(line_level2)
                            break
            else:
                if count==1:
                    for l, line_level1 in enumerate(current_cont):
                        for line_level2 in line_level1:
                            contour_points.append(line_level2)
                            break

        cont_lines_to_pre_process,first_element = [],0
        for l,cordinate in enumerate(contour_points):
            x,y = cordinate
            # print x,y,l
            if l==0:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                first_element=[x,y]
            elif l==len(contour_points)-1:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                cont_lines_to_pre_process[l-1].append([x,y])
                cont_lines_to_pre_process[l].append(first_element)
            else:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                cont_lines_to_pre_process[l-1].append([x,y])

        cont_points_after_pre_process = []
        min_line_length = int(avg_door_width/20)
        min_length_line,last_element = False,0
        for l,each_line in enumerate(cont_lines_to_pre_process):
            x1,y1 = each_line[0]
            x2,y2 = each_line[1]
            line_length = math.hypot(x2 - x1, y2 - y1)
            if l ==0:
                cont_points_after_pre_process.append(each_line[0])
                if line_length<min_line_length:
                    continue
                else:
                    cont_points_after_pre_process.append(each_line[1])

            elif l== len(cont_lines_to_pre_process)-1:
                if line_length<min_line_length:
                    del cont_points_after_pre_process[-1]
                if min_length_line==True:
                    cont_points_after_pre_process.append(each_line[0])
                cont_points_after_pre_process.append(each_line[1])
            else:
                if line_length>min_line_length:
                    cont_points_after_pre_process.append(each_line[0])
                    cont_points_after_pre_process.append(each_line[1])

        final_contour_points,i_point,last_point = [],0,0
        line_obj = line()
        # contour_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        line_gradient_x , line_gradient_y = 0,0
        line_gradient_empty = False
        for i,current_point in enumerate(cont_points_after_pre_process):
            x1, y1 = cont_points_after_pre_process[i-1]
            x2,y2 = current_point

            if i==0:
                final_contour_points.append(current_point)
            elif i==len(cont_points_after_pre_process)-1:
                final_contour_points.append(current_point)
            else:
                if i==1 or line_gradient_empty:
                    current_line_length = math.hypot(x2 - x1, y2 - y1)
                    if current_line_length>avg_door_width/20:
                        current_gradient_x,current_gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
                        line_gradient_x, line_gradient_y = current_gradient_x,current_gradient_y
                        line_gradient_empty = False
                    else:
                        line_gradient_empty= True
                        continue

                x3,y3 = cont_points_after_pre_process[i+1]
                next_gradient_x,next_gradient_y = line_obj.find_line_gradient(x2,y2,x3,y3)
                x_difference = abs(line_gradient_x-next_gradient_x)
                y_difference = abs(line_gradient_y-next_gradient_y)

                if ~(x_difference < 0.05 and y_difference< 0.05):
                    next_line_length = math.hypot(x3 - x2, y3 - y2)
                    if next_line_length>avg_door_width/20:
                        final_contour_points.append(current_point)
                        line_gradient_x, line_gradient_y = next_gradient_x,next_gradient_y

        # print '-----------final_contour_points-----'
        # for row in final_contour_points:
        #     print row

        new_contour_points  = []
        for p, current_point in enumerate(final_contour_points):
            if p == 0:
                new_contour_points.append(current_point)
            else:
                current_x, current_y = current_point
                last_x, last_y = new_contour_points[-1]
                x_difference = abs(last_x-current_x)
                y_difference = abs(last_y-current_y)
                if x_difference<avg_door_width/15:
                    new_contour_points.append([last_x,current_y])
                elif y_difference<avg_door_width/15:
                    new_contour_points.append([current_x,last_y])
                else:
                    new_contour_points.append(current_point)

        # print '----------new_contour_points----------'
        # for row in new_contour_points:
        #     print new_contour_points

        image_contour_lines,first_element = [],0
        for l,cordinate in enumerate(new_contour_points):
            x,y = cordinate
            if l==0:
                image_contour_lines.append([])
                image_contour_lines[l].append([x,y])
                first_element=[x,y]
            elif l==len(final_contour_points)-1:
                image_contour_lines[l-1].append([x,y])
                if ~(x==first_element[0] and y==first_element[1]):
                    image_contour_lines.append([])
                    image_contour_lines[l].append([x,y])
                    image_contour_lines[l].append(first_element)
            else:
                image_contour_lines.append([])
                image_contour_lines[l].append([x,y])
                image_contour_lines[l-1].append([x,y])
                # last_element = [x,y]
        last_element = -1
        min_length = avg_door_width/20
        image_contour_lines2 = []
        for n,each_line in enumerate(image_contour_lines):
            # print each_line
            x1,y1 = each_line[0]
            x2,y2 = each_line[1]
            line_length = math.hypot(x2 - x1, y2 - y1)
            if n==last_element:
                continue
            elif n==len(image_contour_lines)-1:
                if line_length<min_length:
                    image_contour_lines2[-1][1]=image_contour_lines2[0][0]
                    break
            else:
                if line_length<min_length:
                    image_contour_lines2.append([each_line[0],image_contour_lines[n+1][1]])
                    last_element = n+1
                else:
                    image_contour_lines2.append(each_line)
                    last_element = -1

        # contour_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        # for l,each_line in enumerate(image_contour_lines):
        #     cv2.line(contour_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)

        new_contour_lines = image_contour_lines2

        return new_contour_lines

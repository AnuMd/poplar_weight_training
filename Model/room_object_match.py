__author__ = 'anu'

import re
import cv2
import os
import shutil
import numpy as np

from operator import itemgetter

class room_object_match_class:
    def get_room_contours(self,current_directory,path_gravvitas,only_image_name,image_height,image_width):
        #---object contour extraction from object file
        # path_gravvitas = '/home/ub/Documents/floorplan_recognition/tesimagistrale/test/object_test/match_test/fp_11/'

        object_details_file = open(path_gravvitas+only_image_name+'_2_object_details.txt', 'r')
        object_details_list = []
        for text_line in object_details_file:
            contour_seperate = text_line.split(':')
            if len(contour_seperate)>1:
                object_contour = re.findall(r"[\w']+", contour_seperate[1])
                int_object_details = [int(x) for x in object_contour]
                object_chunks = [int_object_details[x:x+2] for x in xrange(0, len(int_object_details), 2)]
                #---temp fix delete later
                x1,y1 = object_chunks[0]
                x2,y2 = object_chunks[1]
                object_contour_new = [[x1,y1],[x2,y1],[x2,y2],[x1,y2]]
                #---end of temp fix
                object_add_bracket = []
                for each_chunk in object_contour_new:
                    object_add_bracket.append([each_chunk])
                object_contour = np.asarray(object_add_bracket)
                object_details_list.append([contour_seperate[0],object_contour])
        object_details_file.close()

        #---text details extraction from text file
        text_details_file = open(path_gravvitas+only_image_name+'_6_text_details.txt', 'r')
        text_details_list = []
        for text_line in text_details_file:
            detail_seperate = text_line.split(':')
            if len(detail_seperate)>3:
                #--seperate and convert text cordinate to int
                cordinates_string_2 = re.findall(r"[\w']+", detail_seperate[1])
                int_cordinates = [int(x) for x in cordinates_string_2]
                cordintes_chunks = [int_cordinates[x:x+2] for x in xrange(0, len(int_cordinates), 2)]
                cordinate_to_append = 0
                for cord in cordintes_chunks:
                    cordinate_to_append = cord

                #--seperate and convert BB-box to int
                bb_cordinates_string_2 = re.findall(r"[\w']+", detail_seperate[2])
                int_bb_cordinates = [int(x) for x in bb_cordinates_string_2]
                bb_cordintes_chunks = [int_bb_cordinates[x:x+4] for x in xrange(0, len(int_bb_cordinates), 4)]
                bb_cordinate_to_append = 0
                for bb_cord in bb_cordintes_chunks:
                    bb_cordinate_to_append = bb_cord

                confidence_string = re.findall(r"[\w']+", detail_seperate[4])
                conf, dict, org, diff = confidence_string
                extra_text_details = [int(conf), dict, org, int(diff)]
                text_details_list.append([detail_seperate[0].strip(),cordinate_to_append,bb_cordinate_to_append,extra_text_details])
        text_details_file.close()



        #---room contour extraction from text file
        room_details_file = open(path_gravvitas+only_image_name+'_1_room_details.txt', 'r')
        room_details_list = []
        for text_line in room_details_file:
            contour_seperate = text_line.split(':')
            if len(contour_seperate)>3:
                unused_earlier = contour_seperate[0]+':'+contour_seperate[1]
                cordinates = contour_seperate[3]
                contour_details = re.findall(r"[\w']+", contour_seperate[5])
                int_contour_details = [int(x) for x in contour_details]
                chunks = [int_contour_details[x:x+2] for x in xrange(0, len(int_contour_details), 2)]
                add_bracket = []
                for each_chunk in chunks:
                    add_bracket.append([each_chunk])
                room_contour = np.asarray(add_bracket)

                #--connect text with text_details_list
                cordinates_string_2 = re.findall(r"[\w']+", cordinates)
                int_cordinates = [int(x) for x in cordinates_string_2]
                cordintes_chunks = [int_cordinates[x:x+2] for x in xrange(0, len(int_cordinates), 2)]
                room_x,room_y = 0,0
                text_ID = -1
                if len(cordintes_chunks[0])>1:
                    for room_cord in cordintes_chunks:
                        room_x,room_y = room_cord

                    for text_row_num, text_detail in enumerate(text_details_list):
                        text_x, text_y = text_detail[1]
                        if text_x==room_x and text_y==room_y:
                            text_ID = text_row_num
                            break
                room_details_list.append([contour_seperate[2],text_ID,room_contour,unused_earlier,cordinates])

        room_details_file.close()



        #--find objects belonging to each room
        room_objects_match_list = []
        # object_details_list_copy = list(object_details_list)
        for room_row,each_room in enumerate(room_details_list):
            temp_room_objects_match = []
            room_contour = each_room[2]
            for ob_row,each_object in enumerate(object_details_list):
                cnt = each_object[1]
                M = cv2.moments(cnt)
                cx = int(M['m10']/M['m00'])
                cy = int(M['m01']/M['m00'])
                # print cx,cy
                #check if the object_centre_point is within a contour
                dist = cv2.pointPolygonTest(room_contour,(cx,cy),False)
                # print dist
                if dist ==1:
                    temp_room_objects_match.append(ob_row)
            room_objects_match_list.append([room_row,each_room[1],temp_room_objects_match])

        # print '-----------room_details_list-------------'
        # for r in room_details_list:
        #     print r[0],r[1]


        #--create list of files in 'room_furniture/' group
        list_obj_text_files = os.listdir(current_directory+'/Model/room_furniture/')
        list_obj_text_files.sort()
        #--create list to group objects based on .txt file names
        room_groups = []
        for txt_file in list_obj_text_files:
            #--ignore hidden file "txt_file.startswith('.')" and backup files "txt_file.endswith('~')"
            if not txt_file.startswith('.') and not txt_file.endswith('~'):
                #--open file and get its content
                file_content = open(current_directory+'/Model/room_furniture/'+txt_file,'r')
                obj_list_in_file  = []
                for txt_line in file_content:
                    obj_list_in_file.append(re.sub(r'\W+', '', txt_line))
                room_groups.append([txt_file[:-4],obj_list_in_file])

        # print '------room_objects_match_list-------'
        for room in room_objects_match_list:
            # print 'BEFORE', room
            correct_text_name = ''
            #----if room has objects:
            if len(room[2])>0:
                if room[1] != -1:
                    #--get text details
                    text_data = text_details_list[room[1]][3]
                    length_of_word = len(text_details_list[room[1]][0])
                    #--get text confidence, dict value and edit distance
                    text_confidence,dict_val, text_difference = text_data[0],text_data[1],text_data[3]
                    if dict_val=='dict':
                        text_diff_to_length_ratio = int(text_difference)/int(length_of_word)
                        threshold = round(((text_confidence/text_diff_to_length_ratio)/100),2)
                        #--check of text data fulfills conditions
                        #--change
                        if threshold<=2:
                            #--get text name
                            text_name = text_details_list[room[1]][0]
                            #--get matching file content
                            matching_file_content = []
                            for group_row in room_groups:
                                if group_row[0]==text_name:
                                    matching_file_content = group_row[1]
                            #--get list of objects in room
                            obj_list = room[2]
                            #--count number of objects in room
                            no_objects_in_room = len(obj_list)
                            #--count matching number of objects to objects supposed to be in room (from .txt file)
                            no_objects_matched = 0
                            for obj in obj_list:
                                object_name = object_details_list[obj][0]
                                new_object_name = (" ".join(re.findall("[a-zA-Z]+", object_name))).strip()
                                for txt_object in matching_file_content:
                                    if txt_object== new_object_name:
                                        no_objects_matched += 1
                                        break



                            #---Case 02
                            copy_room_groups = []
                            for room_grp in room_groups:
                                copy_room_groups.append([room_grp[0],0])
                            if no_objects_matched != no_objects_in_room:
                                for obj in obj_list:
                                    object_name = object_details_list[obj][0]
                                    new_object_name = (" ".join(re.findall("[a-zA-Z]+", object_name))).strip()
                                    for room_row, room_group_row in enumerate(room_groups):
                                        for txt_object in room_group_row[1]:
                                            if txt_object== new_object_name:
                                                copy_room_groups[room_row][1] += 1
                                                break


                                copy_room_groups.sort(key=itemgetter(1),reverse=True)
                                if text_name != copy_room_groups[0][0]:
                                    correct_text_name = copy_room_groups[0][0]
                elif room[1] == -1:
                    copy_room_groups = []
                    for room_grp in room_groups:
                        copy_room_groups.append([room_grp[0],0])
                    for obj in room[2]:
                        object_name = object_details_list[obj][0]
                        new_object_name = (" ".join(re.findall("[a-zA-Z]+", object_name))).strip()
                        for room_row, room_group_row in enumerate(room_groups):
                            for txt_object in room_group_row[1]:
                                if txt_object== new_object_name:
                                    copy_room_groups[room_row][1] += 1
                                    break
                    copy_room_groups.sort(key=itemgetter(1),reverse=True)
                    correct_text_name = copy_room_groups[0][0]

            if len(correct_text_name)>0:
                new_text_ID = -1
                new_text = ''
                for t_row_num, text_row in enumerate(text_details_list):
                    if text_row[0]==correct_text_name:
                        new_text_ID = t_row_num
                        new_text = text_row[0]
                        break
                if new_text_ID != -1:
                    room[1] = new_text_ID
                    room_details_list[room[0]][1] = new_text_ID
                    room_details_list[room[0]][0] = ' '+new_text

        # print '-----------EDITED room_details_list-------------'
        # for r in room_details_list:
        #     print r[0],r[1]


        room_details_file = open(path_gravvitas+only_image_name+'_1_room_details.txt', 'w')
        room_details_file.write('Total No of Rooms: '+str(len(room_details_list))+'\n')
        for room_line in room_details_list:
            contour_data = ''
            for c,cont_point in enumerate(room_line[2]):
                if c==0:
                    contour_data  = contour_data+'['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                else:
                    contour_data  = contour_data+',['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
            room_details_file.write(str(room_line[3])+':'+str(room_line[0])+':'+str(room_line[4])+': contour_details : '+contour_data+'\n'+'\n')
        room_details_file.close()
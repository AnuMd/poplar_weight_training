__author__ = 'anu'

import cv2
import os
import math
import numpy as np
from operator import itemgetter
from PIL import Image

from open_plan_line import line
from open_plan import planClass

class pre_process_class:
    def image_erosion(self,image,path):
        cv2.imwrite(path+'P-1-before preporcess.png', image)
        img = cv2.imread(path+'P-1-before preporcess.png',0)
        img = abs(255-img)
        cv2.imwrite(path+'P-2-color inverse.png', img)

        kernel = np.ones((2,2),np.uint8)
        erosion = cv2.erode(img,kernel,iterations = 1)
        cv2.imwrite(path+'P-3-AFTER erosion.png', erosion)

        image_to_process = abs(255-erosion)
        cv2.imwrite(path+'P-4-return input.png', image_to_process)

        return image_to_process


    def edit_image(self,image,img_walls_and_doors_color,input_image,path,names,avg_doors,contour_to_delete):
        # input_image = cv2.cvtColor(input_image,cv2.cv.CV_GRAY2BGR)
        cv2.imwrite(path+'room.png', image)
        image_to_process = cv2.imread(path+'room.png',cv2.IMREAD_GRAYSCALE)
        color_image = cv2.imread(path+'room.png',cv2.IMREAD_COLOR)
        cv2.imwrite(path+'B&W'+names+'room.png', image_to_process)
        cv2.imwrite(path+'COLOR'+names+'room.png', color_image)

        img_height,img_width= image_to_process.shape
        ret,thresh = cv2.threshold(image_to_process,127,255,0)
        contours,hierachy = cv2.findContours(thresh,1,2)
        print 'name',names
        print len(contours)
        output = ~(np.zeros((img_height,img_width,3), np.uint8))
        total_length = 0
        for id,cnt in enumerate(contours):
            # print cnt
            for point_id,contour_points in enumerate(cnt):
                if point_id!=len(cnt)-1:
                    current_point = contour_points[0]
                    next_point = cnt[point_id+1][0]
                else:
                    current_point = contour_points[0]
                    next_point = cnt[0][0]

                x1,y1 = current_point
                x2,y2 = next_point
                cont_line_length = math.hypot(x2 - x1, y2 - y1)
                total_length = total_length+ cont_line_length

            if total_length>avg_doors*5:
                cv2.drawContours(output,[cnt],0,(0,0,255),2)
                contour_to_delete= True
                cv2.drawContours(input_image,[cnt],0,(255,255,255),10)
                cv2.imwrite(path+'-'+names+'room222222.png', input_image)
                cv2.imwrite(path+'-'+names+'room.png', output)
                print '------PRINTED GO CHECK------------'
                break
            else:
                cv2.drawContours(output,[cnt],0,(255,0,0),2)





        os.remove(path+'room.png')

        return contour_to_delete,input_image


    def close_outer_contour(self,original_image_path,path,only_image_name):
        door_width = 100
        wall_width = 60
        stefano_gray_image = cv2.imread('/home/ub/Documents/floorplan_recognition/tesimagistrale/test/p_issue/'+only_image_name+'/'+'Stefano_output.png',cv2.IMREAD_GRAYSCALE)


        #-----test 1
        cv2.imwrite(path+only_image_name+'-1-stefano_gray.png', stefano_gray_image)
        stef_image_height,stef_image_width = stefano_gray_image.shape
        original_image = Image.open(original_image_path)
        original_pixels = original_image.load()

        empty_new_image = ~(np.zeros((stef_image_height,stef_image_width,3), np.uint8))
        cv2.imwrite(path+'empty.png', empty_new_image)
        empty_image_read = Image.open(path+'empty.png')
        empty_pixels = empty_image_read.load()

        stefano_image = Image.open(path+only_image_name+'-1-stefano_gray.png')
        width, height = stefano_image.size
        stefano_pixels = stefano_image.load()
        # pixel_data = [[-1 for column in range(width)] for row in range(height)]
        for img_row in range(height-1):
            for img_column in range(width-1):
                # colors go as R,G,B
                color = stefano_pixels[img_column,img_row]
                if color!=0:
                    empty_pixels[img_column,img_row] = (0,0,255)

        empty_image_read.save(path+only_image_name+'-2-outer.png')
        os.remove(path+'empty.png')
        #---end test 1




        #--method 2
        img_height, img_width = stefano_gray_image.shape
        ret,thresh = cv2.threshold(stefano_gray_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        max_contour_area = 0
        outer_contour = 0
        image_border_threshold = 20000
        for c,cnt in enumerate(contours):
            contour_area = cv2.contourArea(cnt)
            image_area = img_width*img_height
            if (image_area-contour_area)>image_border_threshold:
                if contour_area>max_contour_area:
                    outer_contour = cnt
        all_contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        cv2.drawContours(all_contour_image,contours,-1,(0,0,0),1)
        cv2.imwrite(path+only_image_name+'-1-s.png', all_contour_image)

        outer_contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        cv2.drawContours(outer_contour_image,[outer_contour],-1,(0,0,0),2)
        cv2.imwrite(path+only_image_name+'test.png', outer_contour_image)
        outer_image = cv2.imread(path+only_image_name+'test.png',cv2.IMREAD_GRAYSCALE)

        plan_obj = planClass('self.path_output','output_path','open_plan_image','name',False)
        new_contour_lines = plan_obj.improve_outer_contour(outer_image,door_width,False)
        os.remove(path+only_image_name+'test.png')

        new_lines_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        for each_line in new_contour_lines:
            cv2.line(new_lines_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)
        cv2.imwrite(path+only_image_name+'-2-s.png', new_lines_image)

        output_image = all_contour_image.copy()
        line_obj = line()

        #-----possible_door_edges are lines that can create door lines
        possible_door_edges = []
        # print  wall_width*0.9,wall_width*1.2
        for l_number,cont_line in enumerate(new_contour_lines):
            x1,y1 = cont_line[0]
            x2,y2 = cont_line[1]
            cont_line_length = math.hypot(x2 - x1, y2 - y1)
            # print cont_line,cont_line_length
            if wall_width*0.33<cont_line_length<wall_width*0.83:
                possible_door_edges.append([cont_line,l_number])

        possible_door_edges_image = all_contour_image.copy()
        for row in possible_door_edges:
            each_line = row[0]
            cv2.line(possible_door_edges_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),2,cv2.cv.CV_AA)
        cv2.imwrite(path+only_image_name+'-3.png', possible_door_edges_image)

        door_image = all_contour_image.copy()
        confirm_door = []
        for p1,possible_row1 in enumerate(possible_door_edges):
            for p2,possible_row2 in enumerate(possible_door_edges):
                if p2>p1:
                    possible_door_lines = []
                    line_1_number = possible_row1[1]
                    possible_line1 = possible_row1[0]
                    x1,y1 = possible_line1[0]
                    x2,y2 = possible_line1[1]

                    line_2_number = possible_row2[1]
                    possible_line2 = possible_row2[0]
                    x3,y3 = possible_line2[0]
                    x4,y4 = possible_line2[1]

                    door_exists = False
                    current_door = []
                    if door_width*0.4<math.hypot(x3 - x1, y3 - y1)<door_width*1.0:
                        is_door = self.check_is_door(new_contour_lines,line_1_number,line_2_number,x1,y1,x3,y3)
                        if is_door:
                            confirm_door.append([[x1,y1],[x3,y3]])
                            confirm_door.append([[x2,y2],[x4,y4]])
                            current_door.append([[x1,y1],[x3,y3]])
                            current_door.append([[x2,y2],[x4,y4]])
                            door_exists= True

                    if door_width*0.4<math.hypot(x4 - x1, y4 - y1)<door_width*1.0:
                        is_door = self.check_is_door(new_contour_lines,line_1_number,line_2_number,x1,y1,x4,y4)
                        if is_door:
                            confirm_door.append([[x1,y1],[x4,y4]])
                            confirm_door.append([[x2,y2],[x3,y3]])
                            current_door.append([[x1,y1],[x4,y4]])
                            current_door.append([[x2,y2],[x3,y3]])
                            door_exists= True

                    if door_width*0.4<math.hypot(x3 - x2, y3 - y2)<door_width*5:
                        is_door = self.check_is_door(new_contour_lines,line_1_number,line_2_number,x2,y2,x3,y3)
                        if is_door:
                            confirm_door.append([[x2,y2],[x3,y3]])
                            confirm_door.append([[x1,y1],[x4,y4]])
                            current_door.append([[x2,y2],[x3,y3]])
                            current_door.append([[x1,y1],[x4,y4]])
                            door_exists= True

                    if door_width*0.4<math.hypot(x4 - x2, y4 - y2)<door_width*1.0:
                        is_door = self.check_is_door(new_contour_lines,line_1_number,line_2_number,x2,y2,x4,y4)
                        if is_door:
                            confirm_door.append([[x2,y2],[x4,y4]])
                            confirm_door.append([[x1,y1],[x3,y3]])
                            current_door.append([[x2,y2],[x4,y4]])
                            current_door.append([[x1,y1],[x3,y3]])
                            door_exists= True

                    if door_exists== True:
                        cv2.line(door_image,(tuple(possible_line1[0])),(tuple(possible_line1[1])),(255,255,255),2,cv2.cv.CV_AA)
                        cv2.line(door_image,(tuple(possible_line2[0])),(tuple(possible_line2[1])),(255,255,255),2,cv2.cv.CV_AA)
                    for each_line in current_door:
                        cv2.line(door_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),1,cv2.cv.CV_AA)

        cv2.imwrite(path+only_image_name+'-4.png', door_image)

        possible_door_edges_image2 = all_contour_image.copy()
        for row in confirm_door:
            print 'confirm',row
            cv2.line(possible_door_edges_image2,(tuple(row[0])),(tuple(row[1])),(0,0,255),2,cv2.cv.CV_AA)
        cv2.imwrite(path+only_image_name+'-5.png', possible_door_edges_image2)

    def check_is_door(self,new_contour_lines,line_1_number,line_2_number,x1,y1,x3,y3):
        is_door = False
        line_obj = line()
        m,c = line_obj.find_mc_of_line(x1,y1,x3,y3)
        #---find line to focus
        line_to_focus_1 = self.find_lines_beside(new_contour_lines,line_1_number,x1,y1)
        line_to_focus_2 = self.find_lines_beside(new_contour_lines,line_2_number,x3,y3)

        new_x1,new_y1 = line_to_focus_1[0]
        new_x2,new_y2 = line_to_focus_1[1]
        new_x3,new_y3 = line_to_focus_2[0]
        new_x4,new_y4 = line_to_focus_2[1]
        m1,c1 = line_obj.find_mc_of_line(new_x1,new_y1,new_x2,new_y2)
        m2,c2 = line_obj.find_mc_of_line(new_x3,new_y3,new_x4,new_y4)

        if m=='a':
            if m1=='a':
                if m2=='a':
                    is_door = True
                elif abs(m2)>6:
                    is_door = True
            elif abs(m1)>6:
                if m2=='a':
                    is_door = True
                elif abs(m2)>6:
                    is_door = True
        elif abs(m)>6:
            if m1=='a':
                if m2=='a':
                    is_door = True
                elif abs(m2)>6:
                    is_door = True
            elif abs(m1)>6:
                if m2=='a':
                    is_door = True
                elif abs(m2)>6:
                    is_door = True

        elif m!='a' and m1!='a' and m2!='a':
            if -0.4< m1-m<0.15:
                is_door = True
            elif -0.4< m2-m<0.15:
                is_door = True

        return is_door



    def find_lines_beside(self,new_contour_lines,line_number,x,y):
        line_to_focus = 0
        cont_line1 = new_contour_lines[line_number-1]
        cont_line2 = new_contour_lines[line_number+1]
        cont_x1,cont_y1 = cont_line1[0]
        cont_x2,cont_y2 = cont_line1[1]
        cont_x3,cont_y3 = cont_line2[0]
        cont_x4,cont_y4 = cont_line2[1]
        if (cont_x1==x and cont_y1==y) or (cont_x2==x and cont_y2==y):
            line_to_focus = cont_line1
        elif (cont_x3==x and cont_y3==y) or (cont_x4==x and cont_y4==y):
            line_to_focus = cont_line2

        return line_to_focus












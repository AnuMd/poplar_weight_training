__author__ = 'stefano'

#NB id di un arco e indipendente dall'indice assegnato


class Node(object):
    def __init__(self, id):
        self.id = id
        self._edges = []
        self._indices = {}
        #id dell'arco avra come valore nodo.id*100+len(self._list_edge)
    #ATTRIBUTES
        self._list_point = []
        self._type = ''
        self.image = None
        self._x_min = 0
        self._x_max = 0
        self._y_min = 0
        self._y_max = 0


    def get_edge(self, a):
        return self._edges[self._indices[a]]

    def add_edge(self, a):
        if a not in self._indices:
            self._edges.append(a)
            self._indices[a] = len(self._edges) -1
        return self.get_edge(a)

    def edges(self):
        return self._edges[:]

    def remove_edge(self,a):
        if a in self._indices:
            self._edges.remove(a)
            for e in self._edges[self._indices[a]:len(self._edges)]:
                self._indices[e] = self._indices[e] - 1
            del self._indices[a]



    # ATTRIBUTI
    def set_type(self, t):
        self._type = t

    def get_type(self):
        return  self._type

    # METODI COORDINATE
    def set_coordinate(self,x1,y1,x2,y2):

        self._x_min=x1
        self._y_min=y1
        self._x_max=x2
        self._y_max=y2


    def get_coordinate(self):

        return self._x_min, self._y_min, self._x_max, self._y_max


    # METODI IMMAGINE
    def set_image(self,i):
        self._image = i


    def get_image(self):
        return self._image


    # METODI LIST_POINT
    def set_list_point(self,l):
        self._list_point.extend(l)


    def get_list_point(self):
        return self._list_point[:]


    def exetend_list_point(self,l):
        self._list_point.append(l)





    # redefines the operator ==
    def __eq__(self, x):
        return x.id == self.id

    # redefines the operator !=
    def __ne__(self, x):
        return not (x == self)

    # redefines the operator "in" for set
    def __hash__(self):
        return self.id.__hash__()

